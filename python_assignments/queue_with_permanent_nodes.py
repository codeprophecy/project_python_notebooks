class Node:
    def __init__(self, number, permanent= False):
        self.number = number
        self.permanent = permanent
        
    # greater than or equal
    def __ge__(self, other):
        """Override the default Equals behavior"""
        return self.number >= other.number
    
    # less than
    def __lt__(self, other):
        """Override the default Equals behavior"""
        return self.number <= other.number

        
    # toString equivalent
    def __str__(self):
        return 'Node: {self.number}*permanent={self.permanent}'.format(self=self)

class PriorityQueue:
    def __init__(self):
        self.items = []
        
        
    def is_empty(self):
        return not self.items

    

    def insert(self, item, permanent= False):
        
        self.items.append(item)
        
        
    def queue_sort(self):
        temp_list = self.items

        # could implement own sort, but not necessary here
        self.items.sort(reverse=True)
        
        
    # 
    def sample(self,n):
        self.queue_sort()
        temp = []
        for i in range(len(self.items)):
            if(self.items[i].permanent == True):
                temp.append(self.items[i])
        return temp

if __name__ == "__main__":
    nodes = [Node(i) for i in [11, 12, 14, 13, 9 , 10, 4, 6]]
    nodes[6].permanent = True
    nodes[7].permanent = True
    nodes[2].permanent = True

    for i in nodes:
        print(i)

    print("***********************************")
    q = PriorityQueue()   

    for num in nodes:
        q.insert(num)

    print("********************************")
    final_no_redundant = q.sample(3)
    for i in final_no_redundant:
        print(i)