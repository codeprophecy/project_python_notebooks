#!/usr/bin/env python
"""firstcode.py"""

import sys
import re

for line in sys.stdin:
    line = line.strip()
    words = re.split('\W+', line)
    
    for word in words:
        if len(word) > 0:
	    print '%s\t%s' % (word.lower()+str(len(word.lower())), 1)
